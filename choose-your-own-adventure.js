// https://projecteuler.net/problem=1

const sumOfMultiplesOf3And5 = (max) => Array.from(Array(max).keys())    // Generate an array containing [0, max)
    .filter(number => number % 3 == 0 || number % 5 == 0)               // Filter for multiples of 3 and/or 5
    .reduce((prev, curr) => prev + curr, 0)                             // Sum

console.log("sumOfMultiplesOf3And5(10) ->", sumOfMultiplesOf3And5(10))
console.log("sumOfMultiplesOf3And5(10) ->", sumOfMultiplesOf3And5(1000))